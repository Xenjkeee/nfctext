package com.xenjkeee.nfctest

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class User(val id: Int = -1, val role: String = "", val email: String = "",
           val firstName: String = "", secondName: String = "", val group: String = "") {
}
