package com.xenjkeee.nfctest

import android.support.v4.content.ContextCompat
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class AuthorizedUserFragmentUI : AnkoComponent<AuthorizedUserFragment> {
    override fun createView(ui: AnkoContext<AuthorizedUserFragment>) = with(ui) {

        relativeLayout {
            id = R.id.pwfapwfp
            background = ContextCompat.getDrawable(ctx, R.color.white)

            button(R.string.scan_qr) {
                onClick {
                    /*todo owner.*/(ctx as MainActivity).scanQRCode()
                }

            }.lparams {
                alignParentBottom()
                alignParentLeft()
                leftMargin = dip(16)
                bottomMargin = dip(16)
            }

            button(R.string.scan_nfc) {
                onClick {
                    /*todo owner.*/(ctx as MainActivity).scanNFC()

                }
            }.lparams {
                alignParentBottom()
                alignParentRight()
                rightMargin = dip(16)
                bottomMargin = dip(16)
            }

        }
    }
}