package com.xenjkeee.nfctest

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class LectureHall(val id: String = "", val block: String = "", val number: String = "", val qr: String = "") {
    val name: String by lazy {
        block.toUpperCase() + number
    }
}
