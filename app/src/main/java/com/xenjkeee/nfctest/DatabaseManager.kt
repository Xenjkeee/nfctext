package com.xenjkeee.nfctest

import android.util.Log
import com.google.firebase.database.*
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener

class DatabaseManager {
    private var database: DatabaseReference? = null

//    fun <T> observing(initialValue: T,
//                      willSet: () -> Unit = { },
//                      didSet: () -> Unit = { }
//    ) = object : ObservableProperty<T>(initialValue) {
//        override fun beforeChange(property: KProperty<*>, oldValue: T, newValue: T): Boolean =
//                true.apply { willSet() }
//
//        override fun afterChange(property: KProperty<*>, oldValue: T, newValue: T) = didSet()
//    }
//
//    var foo: DatabaseReference? by observing(database, didSet = {
//        foo?.child("users")?.addListenerForSingleValueEvent(
//    })

    init {
        database = FirebaseDatabase.getInstance().reference
    }

    fun addValueEventListenerForLectureHalls(action: (List<LectureHall>?) -> Unit) {
        if (database?.child("lecture_halls") != null)
            addValueEventListenerForList(database?.child("lecture_halls")!!, action)
    }

    fun addValueEventListenerForUsers(uid: String, action: (User?) -> Unit) {
        val ref = database?.child("users")?.child(uid)
        if (ref != null) addValueEventListener<User>(ref, action)
    }

    private inline fun <reified T>addValueEventListenerForList(reference: DatabaseReference, crossinline action: (List<T>?) -> Unit) {
        reference.addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        // Get Post object and use the values to update the UI
                        var buffer = mutableListOf<T>()
                        val iterator = dataSnapshot.children.iterator()
                        while (iterator.hasNext()) {
                            val user = iterator.next().getValue(T::class.java)
                            if (user != null) buffer.add(user)
                        }
                        action(buffer.toList())
                    }
                    override fun onCancelled(p0: DatabaseError?) {
                        Log.d("database", "$p0")
                    }
                })
    }

    private inline fun <reified T>addValueEventListener(reference: DatabaseReference, crossinline action: (T?) -> Unit) {
        reference.addValueEventListener(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        action(dataSnapshot.getValue(T::class.java))
                    }
                    override fun onCancelled(p0: DatabaseError?) {
                        Log.d("database", "$p0")
                    }
                })
    }

//    private fun <T> writeNew(new: T, parentKey: String, key: String) {
//        database?.child("lecture_halls")?.child("0")?.setValue( LectureHall("009", "W", "111", "")  )
////        database?.child(parentKey).child(key).setValue(new)
//    }

}
