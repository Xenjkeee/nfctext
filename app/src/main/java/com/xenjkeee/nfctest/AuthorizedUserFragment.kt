package com.xenjkeee.nfctest

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.AnkoContext

/**
 * @author stas
 * @since 06.02.18.
 */
class AuthorizedUserFragment : Fragment() {
    val ui = AuthorizedUserFragmentUI()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)
        return ui.createView(AnkoContext.create(context, this, false))
    }
}


