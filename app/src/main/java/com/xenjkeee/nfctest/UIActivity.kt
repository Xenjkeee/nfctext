package com.xenjkeee.nfctest

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.widget.*
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class UIActivity : AnkoComponent<MainActivity>, SelfIdentifiable {
    lateinit var userName: TextView
    lateinit var listView: ListView
    lateinit var signInButton: Button
    lateinit var scanQRCodeButton: Button


    override var currentId: Int = 1
    private lateinit var fragmentCOntainer: FrameLayout

    private lateinit var mOwner: MainActivity

    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {


        mOwner = owner
        relativeLayout {

            fragmentCOntainer = frameLayout {
                id = R.id.fragment_container
                background = ContextCompat.getDrawable(ctx, R.color.white)
            }.lparams {
                width = matchParent
                height = matchParent
            }




            listView = listView {
            }.lparams {
                alignParentTop()
                centerHorizontally()
            }
            userName = textView {
                text = "info"
                identify()
            }.lparams {
                centerInParent()
            }
            signInButton = button {
                identify()
                text = "Log In"
                onClick {
                    owner.onAuthClick()
                }
            }.lparams {
                alignParentBottom()
                centerHorizontally()
            }
        }
    }

    fun toggleSignButton(signed: Boolean) {
        signInButton.text = if (signed == true) "Log Out" else "Log In"
    }

    @SuppressLint("SetTextI18n")
    fun updateUserInfo(user: User?) {
        //todo: print user info
        if (user != null) {
            userName.text = "id: ${user.id}  name: ${user.firstName} group: ${user.group}"
        } else {
            userName.text = "user are not exist"
        }
    }


    fun updateLectureHallList(list: List<LectureHall>?) {
        if (list != null) {
            listView.adapter = AnkoAdapter<LectureHall>({ list }) { index, items, view ->
                relativeLayout {
                    textView(items[index].name) {
                        textSize = 25F
                    }.lparams {
                        centerHorizontally()
                        topPadding = 35
                        bottomPadding = 35
                    }
                }
            }
        } else {
            //todo: add placeholder
        }
    }


    fun replaceFragment(fragment: Fragment) {
        mOwner.supportFragmentManager.beginTransaction().replace(fragmentCOntainer.id, fragment).commit()
    }

}

interface SelfIdentifiable {
    var currentId: Int

    fun View.identify() {
        id = currentId++
    }

}

class AnkoAdapter<T>(itemFactory: () -> List<T>, private val viewFactory: Context.(index: Int, items: List<T>, view: View?) -> View) : BaseAdapter() {
    private val items: List<T> by lazy { itemFactory() }

    override fun getView(index: Int, view: View?, viewGroup: ViewGroup?): View {
        return viewGroup!!.context.viewFactory(index, items, view)
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(index: Int): T {
        return items[index]
    }

    override fun getItemId(index: Int): Long {
        return (items[index] as Any).hashCode().toLong() + (index.toLong() * Int.MAX_VALUE)
    }
}
