package com.xenjkeee.nfctest

import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.MifareUltralight
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.*
import java.io.IOException
import java.nio.charset.Charset
import java.util.*

class MainActivity : AppCompatActivity(), AnkoLogger {
    private val ui = UIActivity()

    private var nfcAdapter: NfcAdapter? = null
    private var databaseManager: DatabaseManager? = null

    private val RC_SIGN_IN = 123

    var providers = Arrays.asList(AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()) //todo: add login on phone number

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ui.createView(AnkoContext.Companion.create(this, this, true))
//        ui.updateUserInfo(FirebaseAuth.getInstance().currentUser)

        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        if (nfcAdapter == null) {
            Toast.makeText(this, "NFC not support your device", Toast.LENGTH_LONG).show()
        }

        databaseManager = DatabaseManager()
//        databaseManager?.addValueEventListenerForLectureHalls {
//            ui.updateLectureHallList(it)
//        }

//        databaseManager?.addValueEventListenerForUsers {
//            val user = it?.first {
//                it.email == FirebaseAuth.getInstance().currentUser.email
//            }
//            ui.updateUserInfo(user)
//        }

//        databaseManager?.create()
    }

    private var currentUser: User? = null


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RC_SIGN_IN -> {
                //ui.updateUserInfo(FirebaseAuth.getInstance().currentUser)
                //todo: предоставлять функционал
                val uid = auth?.currentUser?.uid
                if (uid != null) {
                    databaseManager?.addValueEventListenerForUsers(uid) {
                        currentUser = it
                        ui.updateUserInfo(it)
                        ui.toggleSignButton(it != null) // TODO: remove


                        //todo move this ti "onAuthSucceed" action
                        ui.replaceFragment(AuthorizedUserFragment())
                    }
                } else {
                    //todo UnauthorizedFragment
                    ui.updateUserInfo(null)
                }

            }
            QR_CODE_READ_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val contents = data?.getStringExtra("SCAN_RESULT")
                    try {

                        onCodeScaned(contents)
                    } catch (e: IllegalArgumentException) {
                        //todo normal message
                        toast("bad qr")
                    }
                }
                //if (resultCode == Activity.RESULT_CANCELED) {}
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }


    }

    private fun onCodeScaned(contents: String?) {
        val id = contents?.toInt() ?: throw IllegalArgumentException("bad qr")
        handleReadedId(id)
    }

    private fun handleReadedId(id: Int) {
        val checkin = CheckinEntity(id, currentUser?.id, Calendar.getInstance().timeInMillis)
        toast(checkin.toString())


    }

    override fun onResume() {
        super.onResume()
        val nfcIntentFilter = arrayOf(
                IntentFilter(Intent.CATEGORY_DEFAULT),
                IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED),
                IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED),
                IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        )

        val pendingIntent = PendingIntent.getActivity(
                this,
                0,
                Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
                0
        )
        nfcAdapter?.enableForegroundDispatch(
                this,
                pendingIntent,
                nfcIntentFilter,
                null
        )
    }

    override fun onPause() {
        super.onPause()
        nfcAdapter?.disableForegroundDispatch(this)
    }


    override fun onNewIntent(intent: Intent) {
        val tag = intent.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)
        debug("onNewIntent: " + intent.action)
        alert {
            positiveButton("readInfo", { readInfo(tag) })
            negativeButton("writeInfo", { writeInfo(tag, "0001") })
        }.show()
    }

    private val charset = "US-ASCII"

    private fun writeInfo(tag: Tag?, text: String) {
        val ultralight = MifareUltralight.get(tag)
        try {
            // Smallest tag only has 64 bytes
            if (text.length / 4 < 12) {
                ultralight.connect()
                for (i in 0 until text.length / 4) {
                    val end = if ((i + 1) * 4 > text.length) text.length else (i + 1) * 4
                    //Clear out the existing data
                    ultralight.writePage(i + 4, (text.substring(i * 4, end).toByteArray(Charset.forName(charset))))
                    //ultralight.writePage(i + 4, "    ".toByteArray(Charset.forName(charset)))

                    //Write new data


                }
            }
        } catch (e: IOException) {
//            kotlin.error("IOException while closing MifareUltralight...", e)
            Log.d("NFC", e.localizedMessage)
            toast("nfc ioException")
        } finally {
            try {
                ultralight.close()
            } catch (e: IOException) {
//                kotlin.error("IOException while closing MifareUltralight...", e)
                Log.d("NFC", e.localizedMessage)
                toast("nfc ioException2")
            }

        }
    }

    private fun readInfo(tag: Tag?) {
        MifareUltralight.get(tag).apply {
            try {
                connect()
                val id = String(readPages(4), Charset.forName(charset)).substring(0, 4).toInt()
                handleReadedId(id)
            } catch (e: Throwable) {
                toast(e.message ?: "smth went wrong")
            } finally {
                close()
            }
        }
    }

    private var auth: FirebaseAuth? = FirebaseAuth.getInstance()

    fun onAuthClick() {
        if (auth?.currentUser == null) {
            startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                    .setAvailableProviders(providers).build(), RC_SIGN_IN)
        } else {
            auth?.signOut()  //fixme: change on smth like startActivityForResult
            ui.toggleSignButton(auth?.currentUser != null) // TODO: move
        }
    }

    private val QR_CODE_READ_CODE: Int = 1231

    fun scanQRCode() {
        try {
            val intent = Intent("com.google.zxing.client.android.SCAN")
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE") // "PRODUCT_MODE for bar codes
            startActivityForResult(intent, QR_CODE_READ_CODE)
        } catch (e: Exception) {

            val marketUri = Uri.parse("market://details?id=com.google.zxing.client.android")
            val marketIntent = Intent(Intent.ACTION_VIEW, marketUri)
            startActivity(marketIntent)

        }

    }

    fun scanNFC() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}


data class CheckinEntity(val hallId: Int, val userId: Int?, val timestampMillis: Long)

val NfcAdapter.info: String
    get() = "isEnabled $isEnabled isNdefPushEnabled $isNdefPushEnabled"
